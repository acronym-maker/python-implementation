##############################################################################
#                                                                            #
#   AcronymMaker - Create awesome acronyms for your projects!                #
#   Copyright (c) 2020-2021 - Romain Wallon (romain.gael.wallon@gmail.com)   #
#                                                                            #
#   This program is free software: you can redistribute it and/or modify     #
#   it under the terms of the GNU General Public License as published by     #
#   the Free Software Foundation, either version 3 of the License, or        #
#   (at your option) any later version.                                      #
#                                                                            #
#   This program is distributed in the hope that it will be useful,          #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of           #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     #
#   See the GNU General Public License for more details.                     #
#                                                                            #
#   You should have received a copy of the GNU General Public License        #
#   along with this program.                                                 #
#   If not, see <https://www.gnu.org/licenses/>.                             #
#                                                                            #
##############################################################################


"""
This package provides a wide variety of strategies allowing to find acronyms
among a list of words (a.k.a. dictionary) to name a project (e.g., software
programs).
"""


__name__ = 'acronymmaker'
__version__ = '0.1.1'
__date__ = '2020-2021'

__summary__ = 'Create awesome acronyms for your projects!'
__keywords__ = 'acronym acronyms build create make word words'
__uri__ = 'https://gitlab.com/acronym-maker/python-implementation'

__author__ = 'Romain Wallon'
__email__ = 'romain.gael.wallon@gmail.com'

__license__ = 'GNU General Public License, Version 3'
__copyright__ = f'Copyright (c) {__date__} - {__author__}'
