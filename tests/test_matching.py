##############################################################################
#                                                                            #
#   AcronymMaker - Create awesome acronyms for your projects!                #
#   Copyright (c) 2020-2021 - Romain Wallon (romain.gael.wallon@gmail.com)   #
#                                                                            #
#   This program is free software: you can redistribute it and/or modify     #
#   it under the terms of the GNU General Public License as published by     #
#   the Free Software Foundation, either version 3 of the License, or        #
#   (at your option) any later version.                                      #
#                                                                            #
#   This program is distributed in the hope that it will be useful,          #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of           #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     #
#   See the GNU General Public License for more details.                     #
#                                                                            #
#   You should have received a copy of the GNU General Public License        #
#   along with this program.                                                 #
#   If not, see <https://www.gnu.org/licenses/>.                             #
#                                                                            #
##############################################################################


"""
Unit tests for the "matching" module.
"""


from typing import Callable, List, Optional, Set, Tuple
from unittest import TestCase

from acronymmaker.matching import Acronym
from acronymmaker.matching import MatchingStrategy
from acronymmaker.matching import GreedyOrderedMatchingStrategy
from acronymmaker.matching import RegexBasedOrderedMatchingStrategy
from acronymmaker.matching import UnorderedMatchingStrategy
from acronymmaker.selection import select_all_letters, select_first_letter
from acronymmaker.token import Token, TokenBuilder


class _TestMatchingStrategy(TestCase):
    """
    The _TestMatchingStrategy is the parent class for the test cases used to
    test matching strategies.
    """

    def setUp(self) -> None:
        """
        Sets up this test case by initializing the letter selection strategy
        used when building tokens.
        """
        self._selection_strategy = self.create_selection_strategy()

    def create_selection_strategy(self) -> Callable[[str], Set[str]]:
        """
        Creates the letter selection strategy to use when building tokens.

        :return: The selection strategy to use to build tokens.
        """
        raise NotImplementedError('Method "create_selection_strategy()" is abstract')

    def _create_mandatory_token(self, *words: str) -> Token:
        """
        Creates a new mandatory Token.

        :param words: The words of the Token to create.

        :return: The created Token.
        """
        return self._create_token(words, False)

    def _create_optional_token(self, *words: str) -> Token:
        """
        Creates a new optional Token.

        :param words: The words of the Token to create.

        :return: The created Token.
        """
        return self._create_token(words, True)

    def _create_token(self, words: Tuple[str], optional: bool = False) -> Token:
        """
        Creates a new Token.

        :param words: The words of the Token to create.
        :param optional: Whether the Token to create is optional.

        :return: The created Token.
        """
        builder = TokenBuilder(self._selection_strategy)
        for word in words:
            builder.add_word(word)
        builder.set_optional(optional)
        return builder.build()

    def _create_default_tokens(self) -> List[Token]:
        """
        Creates the default Tokens, which are used in most test cases.

        :return: The list of the default Tokens.
        """
        build_create = self._create_mandatory_token('build', 'create')
        amazing_awesome = self._create_mandatory_token('amazing', 'awesome')
        acronym = self._create_mandatory_token('acronyms')
        opt_for = self._create_optional_token('for')
        opt_your = self._create_optional_token('your')
        projects = self._create_mandatory_token('projects')
        return [build_create, amazing_awesome, acronym, opt_for, opt_your, projects]

    def assert_matches(self, matching_strategy: MatchingStrategy,
                       tokens: List[Token], explained_acronym: str,
                       explanations: Optional[List[str]] = None) -> None:
        """
        Checks that an acronym is explained by the specified sequence of
        Tokens.

        :param matching_strategy: The matching strategy used to test whether the
                                  acronym is explained.
        :param tokens: The Tokens with which to perform the test.
        :param explained_acronym: The acronym that should be explained.
        :param explanations: The explanations of the acronym, if testable.
        """
        matching_strategy.set_tokens(tokens)
        acronym = matching_strategy.as_acronym(explained_acronym.lower())
        self.assertIsNotNone(acronym)
        if explanations is None:
            self._assert_is_explained(acronym)
        else:
            self._assert_is_explained_exactly(explained_acronym, explanations, acronym)

    def _assert_is_explained(self, acronym: Acronym) -> None:
        """
        Checks that the given acronym is properly explained.

        :param acronym: The acronym that should be explained by the Tokens.
        """
        for explanation in acronym.get_explanations():
            self._assert_explanation_is_correct(acronym.get_word(), explanation)

    def _assert_explanation_is_correct(self, word: str, explanation: str) -> None:
        """
        Checks that the given explanation indeed explains the given word.

        :param word: The explained word.
        :param explanation: The explanation for the word.
        """
        token_index = 0
        explained_tokens = explanation.split()
        for letter in word:
            if letter.isupper():
                self.assertIn(letter, explained_tokens[token_index])
                token_index += 1
        self.assertEqual(token_index, len(explained_tokens))

    def _assert_is_explained_exactly(self, expected_acronym: str, expected_explanations: List[str],
                                     acronym: Acronym) -> None:
        """
        Checks that the given acronym is exactly explained as described.

        :param expected_acronym: The acronym that should have been found.
        :param expected_explanations: The expected explanations for the acronym.
        :param acronym: The found acronym.
        """
        self.assertEqual(expected_acronym, acronym.get_word())
        self.assertEqual(len(expected_explanations), len(acronym.get_explanations()))
        for explanation in expected_explanations:
            self.assertIn(explanation, acronym.get_explanations())

    def assert_does_not_match(self, matching_strategy: MatchingStrategy,
                              tokens: List[Token], word: str) -> None:
        """
        Checks that the given word is not explained as an acronym by the
        specified sequence of Tokens.

        :param matching_strategy: The matching strategy used to test whether the
                                  word is explained.
        :param tokens: The Tokens with which to perform the test.
        :param word: The word that should not be explained.
        """
        matching_strategy.set_tokens(tokens)
        acronym = matching_strategy.as_acronym(word.lower())
        self.assertIsNone(acronym, f'Word "{word}" is explained: {acronym}')


class _TestMatchingStrategyWithFirstLetterSelectionStrategy(_TestMatchingStrategy):
    """
    The _TestMatchingStrategyWithFirstLetterSelectionStrategy is the parent
    class for the test cases used to test matching strategies while selecting
    only the first letter of the words in the considered tokens.
    """

    def create_selection_strategy(self) -> Callable[[str], Set[str]]:
        """
        Creates the letter selection strategy to use when building tokens.

        :return: The letter selection strategy selecting the first letter from
                 a given word.
        """
        return select_first_letter


class _TestMatchingStrategyWithAllLettersSelectionStrategy(_TestMatchingStrategy):
    """
    The _TestMatchingStrategyWithAllLettersSelectionStrategy is the parent
    class for the test cases used to test matching strategies while selecting
    all the letters of the words in the considered tokens.
    """

    def create_selection_strategy(self) -> Callable[[str], Set[str]]:
        """
        Creates the letter selection strategy to use when building tokens.

        :return: The letter selection strategy selecting all the letters from
                 a given word.
        """
        return select_all_letters


class _TestSelectFirstLetterOrderedMatchingStrategy(_TestMatchingStrategyWithFirstLetterSelectionStrategy):
    """
    The _TestSelectFirstLetterOrderedMatchingStrategy is the parent class for
    the test cases checking matching strategies considering the order of
    the tokens combined with the selection of the first letter of the words
    in the considered tokens.
    """

    def _create_strategy(self, max_consecutive_unused: int, max_unused: int) -> MatchingStrategy:
        """
        Creates the matching strategy to test.

        :param max_consecutive_unused: The maximum number of consecutive
               unused letters in the acronym.
        :param max_unused: The maximum number of overall unused letters in the
               acronym.

        :return: The created matching strategy.
        """
        raise NotImplementedError('Method "_create_strategy()" is abstract')

    def test_matches(self) -> None:
        """
        Test that words that are expected to be explained as acronyms are
        indeed explained.
        """
        tokens = self._create_default_tokens()
        matching_strategy = self._create_strategy(5, 10)
        self.assert_matches(matching_strategy, tokens, 'CAnAPe',
                            ['Create Awesome Acronyms Projects', 'Create Amazing Acronyms Projects'])
        self.assert_matches(matching_strategy, tokens, 'CArAPace',
                            ['Create Awesome Acronyms Projects', 'Create Amazing Acronyms Projects'])
        self.assert_matches(matching_strategy, tokens, 'oCeAnogrAPhy',
                            ['Create Awesome Acronyms Projects', 'Create Amazing Acronyms Projects'])

    def test_do_not_match(self) -> None:
        """
        Tests that words do not match if all mandatory words from the Tokens
        cannot be used to explain letters.
        """
        tokens = self._create_default_tokens()
        matching_strategy = self._create_strategy(5, 10)
        self.assert_does_not_match(matching_strategy, tokens, 'aaron')
        self.assert_does_not_match(matching_strategy, tokens, 'army')
        self.assert_does_not_match(matching_strategy, tokens, 'cataloguing')
        self.assert_does_not_match(matching_strategy, tokens, 'cavity')
        self.assert_does_not_match(matching_strategy, tokens, 'cold')
        self.assert_does_not_match(matching_strategy, tokens, 'earn')
        self.assert_does_not_match(matching_strategy, tokens, 'feature')
        self.assert_does_not_match(matching_strategy, tokens, 'mail')
        self.assert_does_not_match(matching_strategy, tokens, 'playback')
        self.assert_does_not_match(matching_strategy, tokens, 'supersaturated')
        self.assert_does_not_match(matching_strategy, tokens, 'telepathically')
        self.assert_does_not_match(matching_strategy, tokens, 'unmusical')
        self.assert_does_not_match(matching_strategy, tokens, 'welcome')

    def test_do_not_match_consecutive_unused(self) -> None:
        """
        Tests that words do not match if there are too many consecutive unexplained
        letters.
        """
        tokens = self._create_default_tokens()
        matching_strategy = self._create_strategy(1, 10)
        self.assert_does_not_match(matching_strategy, tokens, 'oceanography')

    def test_do_not_match_total_unused(self) -> None:
        """
        Tests that words do not match if there are too many overall unexplained
        letters.
        """
        tokens = self._create_default_tokens()
        matching_strategy = self._create_strategy(3, 3)
        self.assert_does_not_match(matching_strategy, tokens, 'carapace')


class TestSelectFirstLetterGreedyOrderedMatchingStrategy(_TestSelectFirstLetterOrderedMatchingStrategy):
    """
    The TestSelectFirstLetterGreedyOrderedMatchingStrategy tests the
    GreedyOrderedMatchingStrategy combined with the selection of the first
    letter of the words in the considered tokens.
    """

    def _create_strategy(self, max_consecutive_unused: int, max_unused: int) -> MatchingStrategy:
        """
        Creates the matching strategy to test.

        :param max_consecutive_unused: The maximum number of consecutive
               unused letters in the acronym.
        :param max_unused: The maximum number of overall unused letters in the
               acronym.

        :return: An instance of GreedyOrderedMatchingStrategy.
        """
        return GreedyOrderedMatchingStrategy(max_consecutive_unused, max_unused)


class TestSelectFirstLetterRegexOrderedMatchingStrategy(_TestSelectFirstLetterOrderedMatchingStrategy):
    """
    The TestSelectFirstLetterRegexOrderedMatchingStrategy tests the
    RegexBasedOrderedMatchingStrategy combined with the selection of the first
    letter of the words in the considered tokens.
    """

    def _create_strategy(self, max_consecutive_unused: int, max_unused: int) -> MatchingStrategy:
        """
        Creates the matching strategy to test.

        :param max_consecutive_unused: The maximum number of consecutive
               unused letters in the acronym.
        :param max_unused: The maximum number of overall unused letters in the
               acronym.

        :return: An instance of RegexBasedOrderedMatchingStrategy.
        """
        return RegexBasedOrderedMatchingStrategy(max_consecutive_unused, max_unused)


class TestSelectFirstLetterUnorderedMatchingStrategy(_TestMatchingStrategyWithFirstLetterSelectionStrategy):
    """
    The TestSelectFirstLetterUnorderedMatchingStrategy tests the
    UnorderedMatchingStrategy combined with the selection of the first
    letter of the words in the considered tokens.
    """

    def test_match(self) -> None:
        """
        Test that words that are expected to be explained as acronyms are
        indeed explained.
        """
        matching_strategy = UnorderedMatchingStrategy(5, 10)
        tokens = self._create_default_tokens()
        self.assert_matches(matching_strategy, tokens, 'canape')
        self.assert_matches(matching_strategy, tokens, 'carapace')
        self.assert_matches(matching_strategy, tokens, 'oceanography')
        self.assert_matches(matching_strategy, tokens, 'playback')
        self.assert_matches(matching_strategy, tokens, 'telepathically')

    def test_do_not_match(self) -> None:
        """
        Tests that words do not match if all mandatory words from the Tokens
        cannot be used to explain letters.
        """
        tokens = self._create_default_tokens()
        matching_strategy = UnorderedMatchingStrategy(5, 10)
        self.assert_does_not_match(matching_strategy, tokens, 'aaron')
        self.assert_does_not_match(matching_strategy, tokens, 'army')
        self.assert_does_not_match(matching_strategy, tokens, 'cataloguing')
        self.assert_does_not_match(matching_strategy, tokens, 'cavity')
        self.assert_does_not_match(matching_strategy, tokens, 'cold')
        self.assert_does_not_match(matching_strategy, tokens, 'earn')
        self.assert_does_not_match(matching_strategy, tokens, 'feature')
        self.assert_does_not_match(matching_strategy, tokens, 'mail')
        self.assert_does_not_match(matching_strategy, tokens, 'supersaturated')
        self.assert_does_not_match(matching_strategy, tokens, 'unmusical')
        self.assert_does_not_match(matching_strategy, tokens, 'welcome')

    def test_do_not_match_consecutive_unused(self) -> None:
        """
        Tests that words do not match if there are too many consecutive unexplained
        letters.
        """
        tokens = self._create_default_tokens()
        matching_strategy = UnorderedMatchingStrategy(1, 10)
        self.assert_does_not_match(matching_strategy, tokens, 'oceanography')
        self.assert_does_not_match(matching_strategy, tokens, 'supersaturated')

    def test_do_not_match_total_unused(self) -> None:
        """
        Tests that words do not match if there are too many overall unexplained
        letters.
        """
        tokens = self._create_default_tokens()
        matching_strategy = UnorderedMatchingStrategy(3, 3)
        self.assert_does_not_match(matching_strategy, tokens, 'carapace')
        self.assert_does_not_match(matching_strategy, tokens, 'cataloguing')
        self.assert_does_not_match(matching_strategy, tokens, 'supersaturated')


class TestSelectAllLettersGreedyOrderedMatchingStrategy(_TestMatchingStrategyWithAllLettersSelectionStrategy):
    """
    The TestSelectAllLettersGreedyOrderedMatchingStrategy tests the
    GreedyOrderedMatchingStrategy combined with the selection of all the
    letters of the words in the considered tokens.
    """

    def test_match(self) -> None:
        """
        Test that words that are expected to be explained as acronyms are
        indeed explained.
        """
        matching_strategy = GreedyOrderedMatchingStrategy(5, 10)
        tokens = self._create_default_tokens()
        self.assert_matches(matching_strategy, tokens, 'CANaPe',
                            ['Create Amazing acroNyms Projects', 'Create Awesome acroNyms Projects'])
        self.assert_matches(matching_strategy, tokens, 'CARaPace',
                            ['Create Amazing acRonyms Projects', 'Create Awesome acRonyms Projects'])
        self.assert_matches(matching_strategy, tokens, 'fEAtuRE',
                            ['crEate Amazing acRonyms projEcts', 'crEate Awesome acRonyms projEcts'])
        self.assert_matches(matching_strategy, tokens, 'pLAYbaCk',
                            ['buiLd Amazing acronYms projeCts', 'buiLd Awesome acronYms projeCts'])
        self.assert_matches(matching_strategy, tokens, 'wElcOME',
                            ['crEate awesOme acronyMs projEcts'])
        self.assert_matches(matching_strategy, tokens, 'yEARbOoks',
                            ['crEate Amazing acRonyms prOjects', 'crEate Awesome acRonyms prOjects'])
        self.assert_matches(matching_strategy, tokens, 'zEAlOUSly',
                            ['crEate Amazing acrOnyms yoUr projectS', 'crEate Awesome acrOnyms yoUr projectS'])

    def test_do_not_match(self) -> None:
        """
        Tests that words do not match if all mandatory words from the Tokens
        cannot be used to explain letters.
        """
        tokens = self._create_default_tokens()
        matching_strategy = GreedyOrderedMatchingStrategy(5, 10)
        self.assert_does_not_match(matching_strategy, tokens, 'army')
        self.assert_does_not_match(matching_strategy, tokens, 'cavity')
        self.assert_does_not_match(matching_strategy, tokens, 'cold')
        self.assert_does_not_match(matching_strategy, tokens, 'earn')
        self.assert_does_not_match(matching_strategy, tokens, 'mail')
        self.assert_does_not_match(matching_strategy, tokens, 'supersaturated')

    def test_do_not_match_consecutive_unused(self) -> None:
        """
        Tests that words do not match if there are too many consecutive unexplained
        letters.
        """
        tokens = self._create_default_tokens()
        matching_strategy = GreedyOrderedMatchingStrategy(1, 10)
        self.assert_does_not_match(matching_strategy, tokens, 'carapace')
        self.assert_does_not_match(matching_strategy, tokens, 'feature')
        self.assert_does_not_match(matching_strategy, tokens, 'playback')
        self.assert_does_not_match(matching_strategy, tokens, 'welcome')
        self.assert_does_not_match(matching_strategy, tokens, 'yearbooks')
        self.assert_does_not_match(matching_strategy, tokens, 'zealously')

    def test_do_not_match_total_unused(self) -> None:
        """
        Tests that words do not match if there are too many overall unexplained
        letters.
        """
        tokens = self._create_default_tokens()
        matching_strategy = GreedyOrderedMatchingStrategy(3, 3)
        self.assert_does_not_match(matching_strategy, tokens, 'carapace')
        self.assert_does_not_match(matching_strategy, tokens, 'playback')
        self.assert_does_not_match(matching_strategy, tokens, 'yearbooks')
        self.assert_does_not_match(matching_strategy, tokens, 'zealously')


class TestSelectAllLettersRegexOrderedMatchingStrategy(_TestMatchingStrategyWithAllLettersSelectionStrategy):
    """
    The TestSelectAllLettersRegexOrderedMatchingStrategy tests the
    RegexBasedOrderedMatchingStrategy combined with the selection of all the
    letters of the words in the considered tokens.
    """

    def test_match(self) -> None:
        """
        Test that words that are expected to be explained as acronyms are
        indeed explained.
        """
        matching_strategy = RegexBasedOrderedMatchingStrategy(5, 10)
        tokens = self._create_default_tokens()
        self.assert_matches(matching_strategy, tokens, 'CANaPe',
                            ['Create Amazing acroNyms Projects', 'Create Awesome acroNyms Projects'])
        self.assert_matches(matching_strategy, tokens, 'CARaPace',
                            ['Create Amazing acRonyms Projects', 'Create Awesome acRonyms Projects'])
        self.assert_matches(matching_strategy, tokens, 'pLAYbaCk',
                            ['buiLd Amazing acronYms projeCts', 'buiLd Awesome acronYms projeCts'])
        self.assert_matches(matching_strategy, tokens, 'wElcOME',
                            ['crEate awesOme acronyMs projEcts'])
        self.assert_matches(matching_strategy, tokens, 'yEARbOOkS',
                            ['crEate Amazing acRonyms fOr yOur projectS', 'crEate Awesome acRonyms fOr yOur projectS'])
        self.assert_matches(matching_strategy, tokens, 'zEAlOUSly',
                            ['crEate Amazing acrOnyms yoUr projectS', 'crEate Awesome acrOnyms yoUr projectS'])

    def test_do_not_match(self) -> None:
        """
        Tests that words do not match if all mandatory words from the Tokens
        cannot be used to explain letters.
        """
        tokens = self._create_default_tokens()
        matching_strategy = RegexBasedOrderedMatchingStrategy(5, 10)
        self.assert_does_not_match(matching_strategy, tokens, 'army')
        self.assert_does_not_match(matching_strategy, tokens, 'cavity')
        self.assert_does_not_match(matching_strategy, tokens, 'cold')
        self.assert_does_not_match(matching_strategy, tokens, 'earn')
        self.assert_does_not_match(matching_strategy, tokens, 'mail')

    def test_do_not_match_consecutive_unused(self) -> None:
        """
        Tests that words do not match if there are too many consecutive unexplained
        letters.
        """
        tokens = self._create_default_tokens()
        matching_strategy = RegexBasedOrderedMatchingStrategy(1, 10)
        self.assert_does_not_match(matching_strategy, tokens, 'feature')
        self.assert_does_not_match(matching_strategy, tokens, 'playback')
        self.assert_does_not_match(matching_strategy, tokens, 'welcome')
        self.assert_does_not_match(matching_strategy, tokens, 'zealously')

    def test_do_not_match_total_unused(self) -> None:
        """
        Tests that words do not match if there are too many overall unexplained
        letters.
        """
        tokens = self._create_default_tokens()
        matching_strategy = RegexBasedOrderedMatchingStrategy(2, 2)
        self.assert_does_not_match(matching_strategy, tokens, 'carapace')
        self.assert_does_not_match(matching_strategy, tokens, 'playback')
        self.assert_does_not_match(matching_strategy, tokens, 'zealously')


class TestSelectAllLettersUnorderedMatchingStrategy(_TestMatchingStrategyWithAllLettersSelectionStrategy):
    """
    The TestSelectAllLettersUnorderedMatchingStrategy tests the
    UnorderedMatchingStrategy combined with the selection of all the
    letters of the words in the considered tokens.
    """

    def test_match(self) -> None:
        """
        Test that words that are expected to be explained as acronyms are
        indeed explained.
        """
        matching_strategy = UnorderedMatchingStrategy(10, 15)
        tokens = self._create_default_tokens()
        self.assert_matches(matching_strategy, tokens, 'canape')
        self.assert_matches(matching_strategy, tokens, 'carapace')
        self.assert_matches(matching_strategy, tokens, 'cavity')
        self.assert_matches(matching_strategy, tokens, 'cataloguing')
        self.assert_matches(matching_strategy, tokens, 'caterpillar')
        self.assert_matches(matching_strategy, tokens, 'catheters')
        self.assert_matches(matching_strategy, tokens, 'oceanography')
        self.assert_matches(matching_strategy, tokens, 'playback')
        self.assert_matches(matching_strategy, tokens, 'ratatouille')
        self.assert_matches(matching_strategy, tokens, 'supersaturated')
        self.assert_matches(matching_strategy, tokens, 'telepathically')
        self.assert_matches(matching_strategy, tokens, 'unmusical')
        self.assert_matches(matching_strategy, tokens, 'welcome')
        self.assert_matches(matching_strategy, tokens, 'yearbooks')
        self.assert_matches(matching_strategy, tokens, 'zealously')

    def test_do_not_match(self) -> None:
        """
        Tests that words do not match if all mandatory words from the Tokens
        cannot be used to explain letters.
        """
        tokens = self._create_default_tokens()
        matching_strategy = UnorderedMatchingStrategy(5, 10)
        self.assert_does_not_match(matching_strategy, tokens, 'able')
        self.assert_does_not_match(matching_strategy, tokens, 'act')
        self.assert_does_not_match(matching_strategy, tokens, 'cold')
        self.assert_does_not_match(matching_strategy, tokens, 'mail')

    def test_do_not_match_consecutive_unused(self) -> None:
        """
        Tests that words do not match if there are too many consecutive unexplained
        letters.
        """
        tokens = self._create_default_tokens()
        matching_strategy = UnorderedMatchingStrategy(1, 10)
        self.assert_does_not_match(matching_strategy, tokens, 'caterpillar')
        self.assert_does_not_match(matching_strategy, tokens, 'oceanography')
        self.assert_does_not_match(matching_strategy, tokens, 'supersaturated')
        self.assert_does_not_match(matching_strategy, tokens, 'telepathically')

    def test_do_not_match_total_unused(self) -> None:
        """
        Tests that words do not match if there are too many overall unexplained
        letters.
        """
        tokens = self._create_default_tokens()
        matching_strategy = UnorderedMatchingStrategy(3, 3)
        self.assert_does_not_match(matching_strategy, tokens, 'cataloguing')
        self.assert_does_not_match(matching_strategy, tokens, 'oceanography')
        self.assert_does_not_match(matching_strategy, tokens, 'supersaturated')
        self.assert_does_not_match(matching_strategy, tokens, 'telepathically')
