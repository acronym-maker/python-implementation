##############################################################################
#                                                                            #
#   AcronymMaker - Create awesome acronyms for your projects!                #
#   Copyright (c) 2020-2021 - Romain Wallon (romain.gael.wallon@gmail.com)   #
#                                                                            #
#   This program is free software: you can redistribute it and/or modify     #
#   it under the terms of the GNU General Public License as published by     #
#   the Free Software Foundation, either version 3 of the License, or        #
#   (at your option) any later version.                                      #
#                                                                            #
#   This program is distributed in the hope that it will be useful,          #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of           #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     #
#   See the GNU General Public License for more details.                     #
#                                                                            #
#   You should have received a copy of the GNU General Public License        #
#   along with this program.                                                 #
#   If not, see <https://www.gnu.org/licenses/>.                             #
#                                                                            #
##############################################################################


"""
Unit tests for the "token" module.
"""


from typing import Callable, List, Set
from unittest import TestCase

from acronymmaker.selection import select_all_letters, select_first_letter
from acronymmaker.token import MatchingToken, Token, TokenBuilder


class _TestToken(TestCase):
    """
    The _TestToken is the parent class for the test cases used to test the behavior
    of instances of Token.
    """

    def setUp(self) -> None:
        """
        Initializes the Tokens to test.
        """
        # Setting up the words of the Tokens.
        builder = TokenBuilder(self.create_strategy())
        builder.add_word('Lighten')
        builder.add_word('painstaking')
        builder.add_word('SPOTLESS')

        # Building a mandatory Token.
        self._mandatory_token = builder.build()

        # Building an optional Token.
        builder.set_optional(True)
        self._optional_token = builder.build()

    def create_strategy(self) -> Callable[[str], Set[str]]:
        """
        Creates the letter selection strategy to use when building the Tokens
        to test.

        :return: The created letter selection strategy.
        """
        raise NotImplementedError('Method "create_strategy()" is abstract')

    def test_optional(self) -> None:
        """
        Tests that the optionality of the Tokens is the expected one.
        """
        self.assertFalse(self._mandatory_token.is_optional())
        self.assertTrue(self._optional_token.is_optional())

    def test_contains(self) -> None:
        """
        Tests that all expected letters are "in" the Tokens.
        """
        self._internal_test_contains(self._mandatory_token)
        self._internal_test_contains(self._optional_token)

    def _internal_test_contains(self, token: Token) -> None:
        """
        Tests that all expected letters are "in" the given Token.

        :param token: The Token under test.
        """
        raise NotImplementedError('Method "_internal_test_contains()" is abstract')

    def test_iter(self) -> None:
        """
        Tests that the iteration over the letters of the Tokens finds exactly
        all expected letters.
        """
        self._internal_test_iter(self._mandatory_token)
        self._internal_test_iter(self._optional_token)

    def _internal_test_iter(self, token: Token) -> None:
        """
        Tests that the iteration over the letters of the given Token finds exactly
        all expected letters.

        :param token: The Token under test.
        """
        raise NotImplementedError('Method "_internal_test_iter()" is abstract')

    def test_words_with_letter(self) -> None:
        """
        Tests that getting the words containing a given letter in the Tokens gives
        the expected words.
        """
        self._internal_test_words_with_letter(self._mandatory_token)
        self._internal_test_words_with_letter(self._optional_token)

    def _internal_test_words_with_letter(self, token: Token) -> None:
        """
        Tests that getting the words containing a given letter in the given Token
        gives the expected words.

        :param token: The Token under test.
        """
        raise NotImplementedError('Method "_internal_test_words_with_letter()" is abstract')

    def test_does_not_contain(self) -> None:
        """
        Tests that letters which are not expected to be selected are not "in"
        the Tokens.
        """
        self._internal_test_does_not_contain(self._mandatory_token)
        self._internal_test_does_not_contain(self._optional_token)

    def _internal_test_does_not_contain(self, token: Token) -> None:
        """
        Tests that letters which are not expected to be selected are not "in"
        the given Token.

        :param token: The Token under test.
        """
        raise NotImplementedError('Method "_internal_test_does_not_contain()" is abstract')

    def test_error_letter_not_contained(self) -> None:
        """
        Tests that getting the words containing a letter fails if the letter
        does not appear in the Tokens.
        """
        self._internal_test_error_letter_not_contained(self._mandatory_token)
        self._internal_test_error_letter_not_contained(self._optional_token)

    def _internal_test_error_letter_not_contained(self, token: Token) -> None:
        """
        Tests that getting the words containing a letter fails if the letter
        does not appear in the given Token.

        :param token: The Token under test.
        """
        raise NotImplementedError('Method "_internal_test_error_letter_not_contained()" is abstract')

    def test_string_representation(self) -> None:
        """
        Tests that the string representation of the Tokens is the expected one.
        """
        self.assertEqual('(lighten or painstaking or spotless)',
                         str(self._mandatory_token))
        self.assertEqual('(lighten or painstaking or spotless)?',
                         str(self._optional_token))


class TestTokenWithFirstLetter(_TestToken):
    """
    The TestTokenWithFirstLetter is the test case for checking the behavior of
    instances of Token in which only the first letter of each word is selected.
    """

    def create_strategy(self) -> Callable[[str], Set[str]]:
        """
        Creates the letter selection strategy to use when building the Token
        to test.

        :return: The letter selection strategy selecting only the first letter
                 from a given word.
        """
        return select_first_letter

    def _internal_test_contains(self, token: Token) -> None:
        """
        Tests that all expected letters are "in" the given Token.

        :param token: The Token under test.
        """
        for letter in 'lps':
            self.assertIn(letter, token)

    def _internal_test_iter(self, token: Token) -> None:
        """
        Tests that the iteration over the letters of the given Token finds exactly
        all expected letters.

        :param token: The Token under test.
        """
        expected_letters = set('lps')
        actual_letters = set()
        for letter in token:
            actual_letters.add(letter)
        self.assertEqual(expected_letters, actual_letters)

    def _internal_test_words_with_letter(self, token: Token) -> None:
        """
        Tests that getting the words containing a given letter in the given Token
        gives the expected words.

        :param token: The Token under test.
        """
        with_l = token.words_with('l')
        self.assertEqual(1, len(with_l))
        self.assertIn('lighten', with_l)

        with_p = token.words_with('p')
        self.assertEqual(1, len(with_p))
        self.assertIn('painstaking', with_p)

        with_s = token.words_with('s')
        self.assertEqual(1, len(with_s))
        self.assertIn('spotless', with_s)

    def _internal_test_does_not_contain(self, token: Token) -> None:
        """
        Tests that letters which are not expected to be selected are not "in"
        the given Token.

        :param token: The Token under test.
        """
        for letter in 'abcdefghijkmnoqrtuvwxyz':
            self.assertNotIn(letter, token)

    def _internal_test_error_letter_not_contained(self, token: Token) -> None:
        """
        Tests that getting the words containing a letter fails if the letter
        does not appear in the given Token.

        :param token: The Token under test.
        """
        for letter in 'abcdefghijkmnoqrtuvwxyz':
            self.assertRaises(ValueError, lambda: token.words_with(letter))


class TestTokenWithAllLetters(_TestToken):
    """
    The TestTokenWithAllLetters is the test case for checking the behavior of
    instances of Token in which all the letters of each word are selected.
    """

    def create_strategy(self) -> Callable[[str], Set[str]]:
        """
        Creates the letter selection strategy to use when building the Token
        to test.

        :return: The letter selection strategy selecting all the letters from
                 a given word.
        """
        return select_all_letters

    def _internal_test_contains(self, token: Token) -> None:
        """
        Tests that all expected letters are "in" the given Token.

        :param token: The Token under test.
        """
        for letter in 'aeghiklnopst':
            self.assertIn(letter, token)

    def _internal_test_iter(self, token: Token) -> None:
        """
        Tests that the iteration over the letters of the given Token finds exactly
        all expected letters.

        :param token: The Token under test.
        """
        expected_letters = set('aeghiklnopst')
        actual_letters = set()
        for letter in token:
            actual_letters.add(letter)
        self.assertEqual(expected_letters, actual_letters)

    def _internal_test_words_with_letter(self, token: Token) -> None:
        """
        Tests that getting the words containing a given letter in the given Token
        gives the expected words.

        :param token: The Token under test.
        """
        with_a = token.words_with('a')
        self.assertEqual(1, len(with_a))
        self.assertIn('painstaking', with_a)

        with_e = token.words_with('e')
        self.assertEqual(2, len(with_e))
        self.assertIn('lighten', with_e)
        self.assertIn('spotless', with_e)

        with_t = token.words_with('t')
        self.assertEqual(3, len(with_t))
        self.assertIn('lighten', with_t)
        self.assertIn('painstaking', with_t)
        self.assertIn('spotless', with_t)

    def _internal_test_does_not_contain(self, token: Token) -> None:
        """
        Tests that letters which are not expected to be selected are not "in"
        the given Token.

        :param token: The Token under test.
        """
        for letter in 'bcdfjmqruvwxyz':
            self.assertNotIn(letter, token)

    def _internal_test_error_letter_not_contained(self, token: Token) -> None:
        """
        Tests that getting the words containing a letter fails if the letter
        does not appear in the given Token.

        :param token: The Token under test.
        """
        for letter in 'bcdfjmqruvwxyz':
            self.assertRaises(ValueError, lambda: token.words_with(letter))


class TestMatchingToken(TestCase):
    """
    The TestMatchingToken is the test case for checking the computation of
    acronym explanations by instances of MatchingToken.
    """

    def setUp(self) -> None:
        """
        Initializes the Token from which to compute a MatchingToken.
        """
        builder = TokenBuilder(select_all_letters)
        builder.add_word('Lighten')
        builder.add_word('painstaking')
        builder.add_word('SPOTLESS')
        self._token = builder.build()

    def test_matching_with_h(self) -> None:
        """
        Tests that the explanations for the letter "h" are properly computed.
        """
        matching_words = self._compute_explanations('h')
        self.assertEqual(1, len(matching_words))
        self.assertIn('ligHten', matching_words)

    def test_matching_with_i(self) -> None:
        """
        Tests that the explanations for the letter "i" are properly computed.
        """
        matching_words = self._compute_explanations('i')
        self.assertEqual(2, len(matching_words))
        self.assertIn('lIghten', matching_words)
        self.assertIn('paInstaking', matching_words)

    def test_matching_with_t(self) -> None:
        """
        Tests that the explanations for the letter "t" are properly computed.
        """
        matching_words = self._compute_explanations('t')
        self.assertEqual(3, len(matching_words))
        self.assertIn('lighTen', matching_words)
        self.assertIn('painsTaking', matching_words)
        self.assertIn('spoTless', matching_words)

    def _compute_explanations(self, letter: str) -> List[str]:
        """
        Computes the explanations for the words in the associated Token which
        contain the given letter.

        :param letter: The letter for which to compute explanations.

        :return: The computed explanations.
        """
        matching_token = MatchingToken(self._token, letter)
        return matching_token.get_explanations()
