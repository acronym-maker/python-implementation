##############################################################################
#                                                                            #
#   AcronymMaker - Create awesome acronyms for your projects!                #
#   Copyright (c) 2020-2021 - Romain Wallon (romain.gael.wallon@gmail.com)   #
#                                                                            #
#   This program is free software: you can redistribute it and/or modify     #
#   it under the terms of the GNU General Public License as published by     #
#   the Free Software Foundation, either version 3 of the License, or        #
#   (at your option) any later version.                                      #
#                                                                            #
#   This program is distributed in the hope that it will be useful,          #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of           #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     #
#   See the GNU General Public License for more details.                     #
#                                                                            #
#   You should have received a copy of the GNU General Public License        #
#   along with this program.                                                 #
#   If not, see <https://www.gnu.org/licenses/>.                             #
#                                                                            #
##############################################################################


"""
Unit tests for the "maker" module.
"""


from typing import Callable, List, Set, Tuple
from unittest import TestCase

from acronymmaker.maker import AcronymMaker
from acronymmaker.matching import Acronym
from acronymmaker.matching import MatchingStrategy
from acronymmaker.matching import GreedyOrderedMatchingStrategy
from acronymmaker.matching import RegexBasedOrderedMatchingStrategy
from acronymmaker.matching import UnorderedMatchingStrategy
from acronymmaker.selection import select_all_letters, select_first_letter


class _TestAcronymMaker(TestCase):
    """
    The _TestAcronymMaker is the parent class for the test cases used to test
    different configurations of AcronymMaker.
    """

    def setUp(self):
        """
        Sets up this test case by initializing the strategies with which the
        instance of AcronymMaker will be initialized.
        """
        self._selection_strategy, self._matching_strategy = self.create_strategies()
        self._known_words = ['able', 'act', 'army', 'canape', 'carapace', 'caterpillar', 'catgut', 'cavity',
                             'cold', 'crop', 'earn', 'feature', 'mail', 'oceanography', 'playback', 'ratatouille',
                             'supersaturated', 'telepathically', 'unmusical', 'welcome', 'yearbooks', 'zealously']

    def create_strategies(self) -> Tuple[Callable[[str], Set[str]], MatchingStrategy]:
        """
        Creates the strategies used to initialize the instance of AcronymMaker.

        :return: The letter selection strategy and matching strategy to use.
        """
        raise NotImplementedError('Method "create_strategies()" is abstract')

    def find_acronyms_for(self, tokens: str) -> List[Acronym]:
        """
        Searches for acronyms that are explained by the given tokens.

        :param tokens: The string of the tokens for which to find acronyms.

        :return: The list of found acronyms.
        """
        acronyms = []
        acronym_maker = AcronymMaker(self._selection_strategy, self._matching_strategy, acronyms.append)
        acronym_maker.add_known_words(self._known_words)
        acronym_maker.find_acronyms_for_string(tokens)
        return acronyms

    def assert_is_explained(self, expected_acronym: str, acronyms: List[Acronym]) -> None:
        """
        Checks that the given acronym has been explained.

        :param expected_acronym: The expected acronym that should have been explained.
        :param acronyms: The list of acronyms that have been found.
        """
        for acronym in acronyms:
            if expected_acronym == acronym.get_word().lower():
                return
        self.fail(f'Acronym "{expected_acronym}" is not explained')

    def assert_is_not_explained(self, word: str, acronyms: List[Acronym]) -> None:
        """
        Checks that the given word has not been explained.

        :param word: The word that should not have been explained.
        :param acronyms: The list of acronyms that have been found.
        """
        for acronym in acronyms:
            if word == acronym.get_word().lower():
                self.fail(f'Word "{word}" is explained: {acronym}')


class TestAcronymMakerSelectFirstLetterGreedyOrderedTokens(_TestAcronymMaker):
    """
    The TestAcronymMakerSelectFirstLetterGreedyOrderedTokens tests an
    instance of AcronymMaker set up with the letter selection strategy selecting
    the first letter of the words and the greedy matching strategy respecting the
    order of the tokens.
    """

    def create_strategies(self) -> Tuple[Callable[[str], Set[str]], MatchingStrategy]:
        """
        Creates the strategies used to initialize the instance of AcronymMaker.

        :return: The letter selection strategy selecting the first letter of
                 the words and the greedy matching strategy respecting the
                 order of the tokens.
        """
        return select_first_letter, GreedyOrderedMatchingStrategy(3, 5)

    def test_explain(self) -> None:
        """
        Tests that the expected words are explained as acronyms.
        """
        acronyms = self.find_acronyms_for('build/create amazing/awesome acronyms for? your? projects')
        self.assertEqual(2, len(acronyms))
        self.assert_is_explained('canape', acronyms)
        self.assert_is_explained('carapace', acronyms)

    def test_do_not_match(self) -> None:
        """
        Tests that words that are not expected to be explained are not
        explained.
        """
        acronyms = self.find_acronyms_for('build/create amazing/awesome acronyms for? your? projects')
        self.assert_is_not_explained('able', acronyms)
        self.assert_is_not_explained('army', acronyms)
        self.assert_is_not_explained('catgut', acronyms)
        self.assert_is_not_explained('cold', acronyms)
        self.assert_is_not_explained('earn', acronyms)
        self.assert_is_not_explained('mail', acronyms)
        self.assert_is_not_explained('playback', acronyms)
        self.assert_is_not_explained('supersaturated', acronyms)
        self.assert_is_not_explained('unmusical', acronyms)
        self.assert_is_not_explained('yearbooks', acronyms)


class TestAcronymMakerSelectFirstLetterRegexOrderedTokens(_TestAcronymMaker):
    """
    The TestAcronymMakerSelectFirstLetterRegexOrderedTokens tests an
    instance of AcronymMaker set up with the letter selection strategy selecting
    the first letter of the words and the regex-based matching strategy
    respecting the order of the tokens.
    """

    def create_strategies(self) -> Tuple[Callable[[str], Set[str]], MatchingStrategy]:
        """
        Creates the strategies used to initialize the instance of AcronymMaker.

        :return: The letter selection strategy selecting the first letter of
                 the words and the regex-based matching strategy respecting the
                 order of the tokens.
        """
        return select_first_letter, RegexBasedOrderedMatchingStrategy(3, 5)

    def test_explain(self) -> None:
        """
        Tests that the expected words are explained as acronyms.
        """
        acronyms = self.find_acronyms_for('build/create amazing/awesome acronyms for? your? projects')
        self.assertEqual(2, len(acronyms))
        self.assert_is_explained('canape', acronyms)
        self.assert_is_explained('carapace', acronyms)

    def test_do_not_match(self) -> None:
        """
        Tests that words that are not expected to be explained are not
        explained.
        """
        acronyms = self.find_acronyms_for('build/create amazing/awesome acronyms for? your? projects')
        self.assert_is_not_explained('act', acronyms)
        self.assert_is_not_explained('caterpillar', acronyms)
        self.assert_is_not_explained('cavity', acronyms)
        self.assert_is_not_explained('crop', acronyms)
        self.assert_is_not_explained('feature', acronyms)
        self.assert_is_not_explained('oceanography', acronyms)
        self.assert_is_not_explained('ratatouille', acronyms)
        self.assert_is_not_explained('telepathically', acronyms)
        self.assert_is_not_explained('welcome', acronyms)
        self.assert_is_not_explained('zealously', acronyms)


class TestAcronymMakerSelectFirstLetterUnorderedTokens(_TestAcronymMaker):
    """
    The TestAcronymMakerSelectFirstLetterUnorderedTokens tests an
    instance of AcronymMaker set up with the letter selection strategy selecting
    the first letter of the words and the matching strategy ignoring the order
    of the tokens.
    """

    def create_strategies(self) -> Tuple[Callable[[str], Set[str]], MatchingStrategy]:
        """
        Creates the strategies used to initialize the instance of AcronymMaker.

        :return: The letter selection strategy selecting the first letter of
                 the words and the matching strategy ignoring the order of the
                 tokens.
        """
        return select_first_letter, UnorderedMatchingStrategy(3, 5)

    def test_explain(self) -> None:
        """
        Tests that the expected words are explained as acronyms.
        """
        acronyms = self.find_acronyms_for('build/create amazing/awesome acronyms for? your? projects')
        self.assertEqual(3, len(acronyms))
        self.assert_is_explained('canape', acronyms)
        self.assert_is_explained('carapace', acronyms)
        self.assert_is_explained('playback', acronyms)

    def test_do_not_match(self) -> None:
        """
        Tests that words that are not expected to be explained are not
        explained.
        """
        acronyms = self.find_acronyms_for('build/create amazing/awesome acronyms for? your? projects')
        self.assert_is_not_explained('able', acronyms)
        self.assert_is_not_explained('act', acronyms)
        self.assert_is_not_explained('army', acronyms)
        self.assert_is_not_explained('catgut', acronyms)
        self.assert_is_not_explained('cavity', acronyms)
        self.assert_is_not_explained('earn', acronyms)
        self.assert_is_not_explained('ratatouille', acronyms)
        self.assert_is_not_explained('unmusical', acronyms)
        self.assert_is_not_explained('welcome', acronyms)
        self.assert_is_not_explained('yearbooks', acronyms)


class TestAcronymMakerSelectAllLettersGreedyOrderedTokens(_TestAcronymMaker):
    """
    The TestAcronymMakerSelectAllLettersGreedyOrderedTokens tests an
    instance of AcronymMaker set up with the letter selection strategy selecting
    all the letters of the words and the greedy matching strategy respecting the
    order of the tokens.
    """

    def create_strategies(self) -> Tuple[Callable[[str], Set[str]], MatchingStrategy]:
        """
        Creates the strategies used to initialize the instance of AcronymMaker.

        :return: The letter selection strategy selecting all the letters of
                 the words and the greedy matching strategy respecting the
                 order of the tokens.
        """
        return select_all_letters, GreedyOrderedMatchingStrategy(3, 5)

    def test_explain(self) -> None:
        """
        Tests that the expected words are explained as acronyms.
        """
        acronyms = self.find_acronyms_for('build/create amazing/awesome acronyms for? your? projects')
        self.assertEqual(7, len(acronyms))
        self.assert_is_explained('canape', acronyms)
        self.assert_is_explained('carapace', acronyms)
        self.assert_is_explained('feature', acronyms)
        self.assert_is_explained('playback', acronyms)
        self.assert_is_explained('welcome', acronyms)
        self.assert_is_explained('yearbooks', acronyms)
        self.assert_is_explained('zealously', acronyms)

    def test_do_not_match(self) -> None:
        """
        Tests that words that are not expected to be explained are not
        explained.
        """
        acronyms = self.find_acronyms_for('build/create amazing/awesome acronyms for? your? projects')
        self.assert_is_not_explained('army', acronyms)
        self.assert_is_not_explained('caterpillar', acronyms)
        self.assert_is_not_explained('catgut', acronyms)
        self.assert_is_not_explained('crop', acronyms)
        self.assert_is_not_explained('earn', acronyms)
        self.assert_is_not_explained('oceanography', acronyms)
        self.assert_is_not_explained('ratatouille', acronyms)
        self.assert_is_not_explained('supersaturated', acronyms)
        self.assert_is_not_explained('telepathically', acronyms)
        self.assert_is_not_explained('unmusical', acronyms)


class TestAcronymMakerSelectAllLettersRegexOrderedTokens(_TestAcronymMaker):
    """
    The TestAcronymMakerSelectAllLettersRegexOrderedTokens tests an
    instance of AcronymMaker set up with the letter selection strategy selecting
    all the letters of the words and the regex-based matching strategy
    respecting the order of the tokens.
    """

    def create_strategies(self) -> Tuple[Callable[[str], Set[str]], MatchingStrategy]:
        """
        Creates the strategies used to initialize the instance of AcronymMaker.

        :return: The letter selection strategy selecting all the letters of
                 the words and the regex-based matching strategy respecting the
                 order of the tokens.
        """
        return select_all_letters, RegexBasedOrderedMatchingStrategy(3, 5)

    def test_explain(self) -> None:
        """
        Tests that the expected words are explained as acronyms.
        """
        acronyms = self.find_acronyms_for('build/create amazing/awesome acronyms for? your? projects')
        self.assertEqual(9, len(acronyms))
        self.assert_is_explained('canape', acronyms)
        self.assert_is_explained('carapace', acronyms)
        self.assert_is_explained('feature', acronyms)
        self.assert_is_explained('playback', acronyms)
        self.assert_is_explained('ratatouille', acronyms)
        self.assert_is_explained('unmusical', acronyms)
        self.assert_is_explained('welcome', acronyms)
        self.assert_is_explained('yearbooks', acronyms)
        self.assert_is_explained('zealously', acronyms)

    def test_do_not_match(self) -> None:
        """
        Tests that words that are not expected to be explained are not
        explained.
        """
        acronyms = self.find_acronyms_for('build/create amazing/awesome acronyms for? your? projects')
        self.assert_is_not_explained('able', acronyms)
        self.assert_is_not_explained('catgut', acronyms)
        self.assert_is_not_explained('cavity', acronyms)
        self.assert_is_not_explained('cold', acronyms)
        self.assert_is_not_explained('crop', acronyms)
        self.assert_is_not_explained('earn', acronyms)
        self.assert_is_not_explained('mail', acronyms)
        self.assert_is_not_explained('oceanography', acronyms)
        self.assert_is_not_explained('supersaturated', acronyms)


class TestAcronymMakerSelectAllLettersUnorderedTokens(_TestAcronymMaker):
    """
    The TestAcronymMakerSelectAllLettersUnorderedTokens tests an
    instance of AcronymMaker set up with the letter selection strategy selecting
    all the letters of the words and the matching strategy ignoring the order
    of the tokens.
    """

    def create_strategies(self) -> Tuple[Callable[[str], Set[str]], MatchingStrategy]:
        """
        Creates the strategies used to initialize the instance of AcronymMaker.

        :return: The letter selection strategy selecting all the letters of
                 the words and the matching strategy ignoring the order of the
                 tokens.
        """
        return select_all_letters, UnorderedMatchingStrategy(10, 15)

    def test_explain(self) -> None:
        """
        Tests that the expected words are explained as acronyms.
        """
        acronyms = self.find_acronyms_for('build/create amazing/awesome acronyms for? your? projects')
        self.assertEqual(18, len(acronyms))
        self.assert_is_explained('army', acronyms)
        self.assert_is_explained('canape', acronyms)
        self.assert_is_explained('carapace', acronyms)
        self.assert_is_explained('caterpillar', acronyms)
        self.assert_is_explained('catgut', acronyms)
        self.assert_is_explained('cavity', acronyms)
        self.assert_is_explained('crop', acronyms)
        self.assert_is_explained('earn', acronyms)
        self.assert_is_explained('feature', acronyms)
        self.assert_is_explained('oceanography', acronyms)
        self.assert_is_explained('playback', acronyms)
        self.assert_is_explained('ratatouille', acronyms)
        self.assert_is_explained('supersaturated', acronyms)
        self.assert_is_explained('telepathically', acronyms)
        self.assert_is_explained('unmusical', acronyms)
        self.assert_is_explained('welcome', acronyms)
        self.assert_is_explained('yearbooks', acronyms)
        self.assert_is_explained('zealously', acronyms)

    def test_do_not_match(self) -> None:
        """
        Tests that words that are not expected to be explained are not
        explained.
        """
        acronyms = self.find_acronyms_for('build/create amazing/awesome acronyms for? your? projects')
        self.assert_is_not_explained('able', acronyms)
        self.assert_is_not_explained('act', acronyms)
        self.assert_is_not_explained('cold', acronyms)
        self.assert_is_not_explained('mail', acronyms)
