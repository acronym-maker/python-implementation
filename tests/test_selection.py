##############################################################################
#                                                                            #
#   AcronymMaker - Create awesome acronyms for your projects!                #
#   Copyright (c) 2020-2021 - Romain Wallon (romain.gael.wallon@gmail.com)   #
#                                                                            #
#   This program is free software: you can redistribute it and/or modify     #
#   it under the terms of the GNU General Public License as published by     #
#   the Free Software Foundation, either version 3 of the License, or        #
#   (at your option) any later version.                                      #
#                                                                            #
#   This program is distributed in the hope that it will be useful,          #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of           #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     #
#   See the GNU General Public License for more details.                     #
#                                                                            #
#   You should have received a copy of the GNU General Public License        #
#   along with this program.                                                 #
#   If not, see <https://www.gnu.org/licenses/>.                             #
#                                                                            #
##############################################################################


"""
Unit tests for the "selection" module.
"""


from typing import Callable, Set
from unittest import TestCase

from acronymmaker.selection import select_all_letters, select_first_letter


class _TestLetterSelectionStrategy(TestCase):
    """
    The _TestLetterSelectionStrategy is the parent class for the test
    cases used to test letter selection strategies.
    """

    def setUp(self) -> None:
        """
        Sets up this test case by initializing the letter selection strategy
        to test.
        """
        self._select = self.create_strategy()

    def create_strategy(self) -> Callable[[str], Set[str]]:
        """
        Creates the letter selection strategy to test.

        :return: The letter selection strategy to test.
        """
        raise NotImplementedError('Method "create_strategy()" is abstract')

    def select_from(self, word: str) -> Set[str]:
        """
        Selects letters from the given word according to the strategy to test.
        This method is defined for convenience, and delegates the work to the
        strategy.

        :param word: The word to select letters from.

        :return: The set of selected letters.
        """
        return self._select(word)

    def test_error_on_empty_word(self) -> None:
        """
        Tests that a ValueError is raised when selecting letters from an
        empty word.
        """
        self.assertRaises(ValueError, lambda: self.select_from(''))


class TestFirstLetterSelectionStrategy(_TestLetterSelectionStrategy):
    """
    The TestFirstLetterSelectionStrategy tests the letter selection
    strategy selecting only the first letter from a given word.
    """

    def create_strategy(self) -> Callable[[str], Set[str]]:
        """
        Creates the letter selection strategy to test.

        :return: The letter selection strategy selecting only the first letter
                 from a given word.
        """
        return select_first_letter

    def test_alert(self) -> None:
        """
        Tests that letters are properly selected from the word "alert".
        """
        letters = self.select_from('alert')
        self.assertEqual(1, len(letters))
        self.assertIn('a', letters)

    def test_bright(self) -> None:
        """
        Tests that letters are properly selected from the word "bright".
        """
        letters = self.select_from('bright')
        self.assertEqual(1, len(letters))
        self.assertIn('b', letters)

    def test_frame(self) -> None:
        """
        Tests that letters are properly selected from the word "frame".
        """
        letters = self.select_from('frame')
        self.assertEqual(1, len(letters))
        self.assertIn('f', letters)

    def test_horn(self) -> None:
        """
        Tests that letters are properly selected from the word "horn".
        """
        letters = self.select_from('horn')
        self.assertEqual(1, len(letters))
        self.assertIn('h', letters)

    def test_hum(self) -> None:
        """
        Tests that letters are properly selected from the word "hum".
        """
        letters = self.select_from('hum')
        self.assertEqual(1, len(letters))
        self.assertIn('h', letters)

    def test_mere(self) -> None:
        """
        Tests that letters are properly selected from the word "mere".
        """
        letters = self.select_from('mere')
        self.assertEqual(1, len(letters))
        self.assertIn('m', letters)

    def test_skillful(self) -> None:
        """
        Tests that letters are properly selected from the word "skillful".
        """
        letters = self.select_from('skillful')
        self.assertEqual(1, len(letters))
        self.assertIn('s', letters)

    def test_spiffy(self) -> None:
        """
        Tests that letters are properly selected from the word "spiffy".
        """
        letters = self.select_from('spiffy')
        self.assertEqual(1, len(letters))
        self.assertIn('s', letters)

    def test_wound(self) -> None:
        """
        Tests that letters are properly selected from the word "wound".
        """
        letters = self.select_from('wound')
        self.assertEqual(1, len(letters))
        self.assertIn('w', letters)

    def test_yarn(self) -> None:
        """
        Tests that letters are properly selected from the word "yarn".
        """
        letters = self.select_from('yarn')
        self.assertEqual(1, len(letters))
        self.assertIn('y', letters)


class TestAllLettersSelectionStrategy(_TestLetterSelectionStrategy):
    """
    The TestAllLettersSelectionStrategy tests the letter selection
    strategy selecting all the letters from a given word.
    """

    def create_strategy(self) -> Callable[[str], Set[str]]:
        """
        Creates the letter selection strategy to test.

        :return: The letter selection strategy selecting all the letters from
                 a given word.
        """
        return select_all_letters

    def test_alert(self) -> None:
        """
        Tests that letters are properly selected from the word "alert".
        """
        letters = self.select_from('alert')
        self.assertEqual(5, len(letters))
        self.assertIn('a', letters)
        self.assertIn('l', letters)
        self.assertIn('e', letters)
        self.assertIn('r', letters)
        self.assertIn('t', letters)

    def test_bright(self) -> None:
        """
        Tests that letters are properly selected from the word "bright".
        """
        letters = self.select_from('bright')
        self.assertEqual(6, len(letters))
        self.assertIn('b', letters)
        self.assertIn('r', letters)
        self.assertIn('i', letters)
        self.assertIn('g', letters)
        self.assertIn('h', letters)
        self.assertIn('t', letters)

    def test_frame(self) -> None:
        """
        Tests that letters are properly selected from the word "frame".
        """
        letters = self.select_from('frame')
        self.assertEqual(5, len(letters))
        self.assertIn('f', letters)
        self.assertIn('r', letters)
        self.assertIn('a', letters)
        self.assertIn('m', letters)
        self.assertIn('e', letters)

    def test_horn(self) -> None:
        """
        Tests that letters are properly selected from the word "horn".
        """
        letters = self.select_from('horn')
        self.assertEqual(4, len(letters))
        self.assertIn('h', letters)
        self.assertIn('o', letters)
        self.assertIn('r', letters)
        self.assertIn('n', letters)

    def test_hum(self) -> None:
        """
        Tests that letters are properly selected from the word "hum".
        """
        letters = self.select_from('hum')
        self.assertEqual(3, len(letters))
        self.assertIn('h', letters)
        self.assertIn('u', letters)
        self.assertIn('m', letters)

    def test_mere(self) -> None:
        """
        Tests that letters are properly selected from the word "mere".
        """
        letters = self.select_from('mere')
        self.assertEqual(3, len(letters))
        self.assertIn('m', letters)
        self.assertIn('e', letters)
        self.assertIn('r', letters)

    def test_skillful(self) -> None:
        """
        Tests that letters are properly selected from the word "skillful".
        """
        letters = self.select_from('skillful')
        self.assertEqual(6, len(letters))
        self.assertIn('s', letters)
        self.assertIn('k', letters)
        self.assertIn('i', letters)
        self.assertIn('l', letters)
        self.assertIn('f', letters)
        self.assertIn('u', letters)

    def test_spiffy(self) -> None:
        """
        Tests that letters are properly selected from the word "spiffy".
        """
        letters = self.select_from('spiffy')
        self.assertEqual(5, len(letters))
        self.assertIn('s', letters)
        self.assertIn('p', letters)
        self.assertIn('i', letters)
        self.assertIn('f', letters)
        self.assertIn('y', letters)

    def test_wound(self) -> None:
        """
        Tests that letters are properly selected from the word "wound".
        """
        letters = self.select_from('wound')
        self.assertEqual(5, len(letters))
        self.assertIn('w', letters)
        self.assertIn('o', letters)
        self.assertIn('u', letters)
        self.assertIn('n', letters)
        self.assertIn('d', letters)

    def test_yarn(self) -> None:
        """
        Tests that letters are properly selected from the word "yarn".
        """
        letters = self.select_from('yarn')
        self.assertEqual(4, len(letters))
        self.assertIn('y', letters)
        self.assertIn('a', letters)
        self.assertIn('r', letters)
        self.assertIn('n', letters)
