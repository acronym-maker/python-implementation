###############################################################################
##          MAKEFILE FOR LINTING AND RUNNING TESTS OF ACRONYM-MAKER          ##
###############################################################################


###############
## VARIABLES ##
###############


# The name of the package to build.
PACKAGE_NAME = acronymmaker


# The version of the package to build.
VERSION = 0.1.1


# The directory containing the executable script(s) of the package.
SCRIPTS = bin


# The directory of the unit tests for the package to build.
TESTS = tests


# The directory where to put build files.
OUTDIR = build


#############
## TARGETS ##
#############


# Declares non-file targets.
.PHONY: test pylint sonar register package upload deploy clean help


##################
## Test Targets ##
##################


# Executes unit tests with code coverage.
test: $(OUTDIR)/nosetests.xml $(OUTDIR)/coverage.xml


# Stores unit tests and code coverage results into files.
$(OUTDIR)/nosetests.xml $(OUTDIR)/coverage.xml: $(OUTDIR) $(PACKAGE_NAME)/*.py $(TESTS)/test*.py
	nosetests --with-xunit --xunit-file $(OUTDIR)/nosetests.xml --with-coverage --cover-xml --cover-xml-file $(OUTDIR)/coverage.xml --cover-package $(PACKAGE_NAME) $(TESTS)/test*.py


#####################
## Linting Targets ##
#####################


# Executes the Pylint static analysis.
pylint: $(OUTDIR)/pylint.out


# Stores the result of the Pylint analysis into a file.
$(OUTDIR)/pylint.out: $(OUTDIR) $(PACKAGE_NAME)/*.py $(SCRIPTS)/* $(TESTS)/*.py
	pylint $(PACKAGE_NAME) $(SCRIPTS) $(TESTS) -r n --msg-template="{path}:{line}: [{msg_id}({symbol}), {obj}] {msg}" | tee $(OUTDIR)/pylint.out


# Executes the SonarQube static analysis.
sonar: test pylint
	sonar-scanner -Dsonar.qualitygate.wait=true


##################
## PyPI Targets ##
##################


# Registers the project on PyPI.
register:
	python3 setup.py register


# Packages the project to upload it on PyPI.
package: dist/$(PACKAGE_NAME)-$(VERSION).tar.gz


# Packages the project into a gzipped-tarball archive.
dist/$(PACKAGE_NAME)-$(VERSION).tar.gz:
	python3 setup.py sdist


# Uploads the project on PyPI.
upload:
	twine upload dist/*


# Performs a clean packaging of the project, and uploads it on PyPI.
deploy: clean package upload


#####################
## Utility Targets ##
#####################


# Creates the directory where to put build files.
$(OUTDIR):
	mkdir -p $(OUTDIR)


# Deletes the build files.
clean:
	rm -rf $(OUTDIR) dist *.egg *.egg-info


# Displays a message describing the available targets.
help:
	@echo
	@echo "Building, Linting and Running Tests of AcronymMaker"
	@echo "==================================================="
	@echo
	@echo "Available targets are:"
	@echo "    - test (default)  -> executes unit tests with code coverage"
	@echo "    - pylint          -> executes the Pylint static analysis"
	@echo "    - sonar           -> executes the SonarQube static analysis"
	@echo "    - register        -> registers the project on PyPI"
	@echo "    - package         -> packages the project to upload it on PyPI"
	@echo "    - upload          -> uploads the project on PyPI"
	@echo "    - deploy          -> packages the project and uploads it on PyPI"
	@echo "    - clean           -> deletes the build files"
	@echo "    - help            -> displays this message"
	@echo
